﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace SoftwareSecurity_3
{
    class SecurityService : IDisposable
    {
        public static readonly byte[] salt = new byte[] { 0x00, 0x42, 0x19, 0x22, 0x43, 0x12, 0x73, 0xee };
        private const int SYMMETRIC_BUFFER = 100;
        private const int KEY_SIZE = 192 / 8;
        private HashAlgorithm hashAlgorithm;
        private SymmetricAlgorithm symmetricAlgorithm;
        private SymmetricAlgorithm keyAlgorithm;

        public SecurityService(HashAlgorithm hashAlgorithm, SymmetricAlgorithm symmetricAlgorithm, SymmetricAlgorithm keyAlgorithm)
        {
            this.hashAlgorithm = hashAlgorithm;
            this.symmetricAlgorithm = symmetricAlgorithm;
            this.keyAlgorithm = keyAlgorithm;
            this.keyAlgorithm.BlockSize = this.keyAlgorithm.LegalBlockSizes[0].MaxSize;
            this.keyAlgorithm.KeySize = this.keyAlgorithm.LegalKeySizes[0].MaxSize;
        }

        public void CreateHash(String sourceFilePath, String hashFilePath)
        {
            using (FileStream file = File.OpenRead(sourceFilePath), hashFile = File.Open(hashFilePath, FileMode.Create)) {
                file.Position = 0;
                byte[] hashValue = hashAlgorithm.ComputeHash(file);
                hashFile.Write(hashValue, 0, hashValue.Length);
            }
        }

        public Boolean CheckHash(String sourceFilePath, String hashFilePath)
        {
            using (FileStream file = File.OpenRead(sourceFilePath), hashFile = File.OpenRead(hashFilePath)) {
                file.Position = 0;
                hashFile.Position = 0;
                byte[] hashValue = hashAlgorithm.ComputeHash(file);
                if (hashValue.Length != hashFile.Length) return false;
                byte[] hashValue2 = new byte[hashValue.Length];
                hashFile.Read(hashValue2, 0, hashValue.Length);
                for (var i = 0; i < hashValue.Length; ++i)
                {
                    if (hashValue[i] != hashValue2[i]) return false;
                }
            }
            return true;
        }

        public void LoadKey(String keyFilePath, String password)
        {
            using (var file = File.OpenRead(keyFilePath))
            {
                file.Position = 0;
                try
                {
                    var rawData = DecryptKey(file, password);
                    //var rawData = new byte[file.Length];
                    //file.Read(rawData, 0, rawData.Length);
                    symmetricAlgorithm.Key = rawData.Take(symmetricAlgorithm.Key.Length).ToArray();
                    symmetricAlgorithm.IV = rawData.Skip(symmetricAlgorithm.Key.Length).Take(symmetricAlgorithm.IV.Length).ToArray();
                }
                catch (Exception e)
                {
                    throw new Exception("Can't decrypt key. Pasword wrong", e);
                }
            }
        }

        public void SaveKey(String keyFilePath, String password)
        {
            using (var file = File.Open(keyFilePath, FileMode.Create))
            {
                var key = symmetricAlgorithm.Key;
                var IV = symmetricAlgorithm.IV;
                var block = key.Concat(IV).ToArray();
                file.SetLength(0);
                EncryptKey(file, block, password);
                //file.Write(block, 0, block.Length);
            }
        }

        private void EncryptKey(Stream openStream, Byte[] block, String password)
        {
            var genKey = new Rfc2898DeriveBytes(password, salt, 1042);
            this.keyAlgorithm.Key = genKey.GetBytes(this.keyAlgorithm.KeySize / 8);
            this.keyAlgorithm.IV = genKey.GetBytes(this.keyAlgorithm.BlockSize / 8);
            var cryptoTransform = this.keyAlgorithm.CreateEncryptor();
            using (var source = new MemoryStream(block))
            {
                using (var cryptoStream = new CryptoStream(source, cryptoTransform, CryptoStreamMode.Read))
                {
                    cryptoStream.CopyTo(openStream);
                }
            }
        }

        private Byte[] DecryptKey(Stream encryptedStream, String password)
        {
            var genKey = new Rfc2898DeriveBytes(password, salt, 1042);
            this.keyAlgorithm.Key = genKey.GetBytes(this.keyAlgorithm.KeySize / 8);
            this.keyAlgorithm.IV = genKey.GetBytes(this.keyAlgorithm.BlockSize / 8);
            var cryptoTransform = this.keyAlgorithm.CreateDecryptor();
            using (var dest = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(dest, cryptoTransform, CryptoStreamMode.Write))
                {
                    encryptedStream.CopyTo(cryptoStream);
                }
                return dest.ToArray();
            }
        }

        public void DecryptFile(String sourceFilePath, String destFilePath)
        {
            Console.WriteLine("Start decrypting file {0}", sourceFilePath);
            try
            {
                var cryptoTransform = this.symmetricAlgorithm.CreateDecryptor(symmetricAlgorithm.Key, symmetricAlgorithm.IV);
                CommonCryptor(cryptoTransform, sourceFilePath, destFilePath);
                Console.WriteLine("\nDecription has been done. Saved in {0}", destFilePath);
            }
            catch (Exception e)
            {
                Console.WriteLine("\nDecription failed due {0}", e);
            }
        }

        public void EncryptFile(String sourceFilePath, String destFilePath)
        {
            Console.WriteLine("Start encrypting file {0}", sourceFilePath);
            var cryptoTransform = this.symmetricAlgorithm.CreateEncryptor(symmetricAlgorithm.Key, symmetricAlgorithm.IV);
            CommonCryptor(cryptoTransform, sourceFilePath, destFilePath);
            Console.WriteLine("\nEncription has been done. Saved in {0}", destFilePath);
        }

        private void CommonCryptor(ICryptoTransform cryptoTransform, String sourceFilePath, String destFilePath)
        {
            using (FileStream source = File.OpenRead(sourceFilePath), dest = File.Open(destFilePath, FileMode.Create))
            {
                source.Position = 0;
                dest.SetLength(0);
                using (CryptoStream cryptoStream = new CryptoStream(dest, cryptoTransform, CryptoStreamMode.Write))
                {
                    source.CopyTo(cryptoStream);
                }
            }
        }

        public void Dispose()
        {
            this.hashAlgorithm.Dispose();
            this.symmetricAlgorithm.Dispose();
        }
    }
}
