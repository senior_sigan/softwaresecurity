﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareSecurity_3
{
    class SecurityFactory
    {
        static public HashAlgorithm buildHashAlgorithm()
        {
            return SHA1.Create();
        }

        static public SymmetricAlgorithm buildSymmetricAlgorithm()
        {
            return TripleDES.Create();
        }

        static public SymmetricAlgorithm buildKeySymmetricAlgorithm()
        {
            return Aes.Create();
        }
    }
}
