﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareSecurity_3
{
    class Program
    {
        static void Help()
        {
            Console.WriteLine("Usage: SoftwareSecurity_1.exe {-e, -d} <source file> <destination file> <key file> <password>");
            Console.WriteLine("OR: build.exe {-c, -h} <source file> <hash file>");
            Console.WriteLine("-e -- encrypt");
            Console.WriteLine("-d -- decrypt");
            Console.WriteLine("-h -- create hash");
            Console.WriteLine("-c -- check hash");
        }

        static void Main(string[] args)
        {
            using (var security = new SecurityService(
                SecurityFactory.buildHashAlgorithm(), 
                SecurityFactory.buildSymmetricAlgorithm(), 
                SecurityFactory.buildSymmetricAlgorithm()))
            {
                if (args.Length == 3)
                {
                    var sourceFilePath = args[1];
                    var hashFilePath = args[2];
                    if (args[0] == "-h")
                    {
                        security.CreateHash(sourceFilePath, hashFilePath);
                        return;
                    }

                    if (args[0] == "-c")
                    {
                        Console.WriteLine(security.CheckHash(sourceFilePath, hashFilePath));
                        return;
                    }
                }

                if (args.Length == 5)
                {
                    var sourceFilePath = args[1];
                    var encFilePath = args[2];
                    var keyPath = args[3];
                    var password = args[4];
                    if (args[0] == "-e")
                    {
                        security.SaveKey(keyPath, password);
                        security.EncryptFile(sourceFilePath, encFilePath);
                        return;
                    }

                    if (args[0] == "-d")
                    {
                        security.LoadKey(keyPath, password);
                        security.DecryptFile(sourceFilePath, encFilePath);
                        return;
                    }
                }
            }
            Help();
        }
    }
}
