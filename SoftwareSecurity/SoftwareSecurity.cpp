#include "stdafx.h"
// https://msdn.microsoft.com/en-us/library/aa382367(v=vs.85).aspx
// https://msdn.microsoft.com/en-us/library/windows/desktop/aa382358(v=vs.85).aspx
// https://msdn.microsoft.com/ru-ru/library/windows/desktop/aa382380(v=vs.85).aspx
// https://msdn.microsoft.com/en-us/library/windows/desktop/aa382044(v=vs.85).aspx

#define ENCRYPT_2 CALG_RC4 
#define ENCRYPT_1 CALG_3DES
#define HASH CALG_SHA
#define PROVIDER_TYPE PROV_RSA_SCHANNEL
#define PROVIDER_NAME MS_DEF_RSA_SCHANNEL_PROV
#define ENCRYPT_BLOCK_SIZE 8
#define KEYLENGTH  0x00800000
#define HASH_LEN 20

HCRYPTPROV hCryptProv = NULL;

bool Initialize(){
	if (CryptAcquireContext(&hCryptProv, NULL, PROVIDER_NAME, PROVIDER_TYPE, 0)) {
		_tprintf(TEXT("Cryptographic provider has been acquired.\n"));
	}
	else {
		DWORD error = GetLastError();
		_ftprintf(stderr, TEXT("Error during acquiring context: %x.\n"), error);
		return FALSE;
	}

	return TRUE;
}

bool MakeHash(LPTSTR lSourceFile, LPTSTR lHashFile) {
	Initialize();

	HCRYPTHASH hHash = NULL;
	HANDLE hSourceFile = INVALID_HANDLE_VALUE;
	HANDLE hHashFile = INVALID_HANDLE_VALUE;
	BYTE bBuffer[1024];
	BYTE bHash[HASH_LEN];
	BOOL bResult = FALSE;
	DWORD cbRead = 0;
	DWORD cbWrite = 0;
	DWORD dHash = HASH_LEN;

	hSourceFile = CreateFile(lSourceFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);
	if (hSourceFile == INVALID_HANDLE_VALUE) {
		_ftprintf(stderr, TEXT("Error oopenning file %s: %x.\n"), lSourceFile, GetLastError());
		return FALSE;
	}

	// generate hash object
	if (CryptCreateHash(hCryptProv, HASH, 0, 0, &hHash)) {
		_tprintf(TEXT("Hash object has been created.\n"));
	}
	else {
		_ftprintf(stderr, TEXT("Error during creating hash object: %x.\n"), GetLastError());
		return FALSE;
	}

	while (bResult = ReadFile(hSourceFile, bBuffer, 1024, &cbRead, NULL))
	{
		if (cbRead == 0) break;

		if (!CryptHashData(hHash, bBuffer, cbRead, 0)) {
			_ftprintf(stderr, TEXT("Error during creating hash of data: %x.\n"), GetLastError());
			return FALSE;
		}
	}

	if (!bResult) {
		_ftprintf(stderr, TEXT("Error during reading file %s: %x.\n"), lSourceFile, GetLastError());
		return FALSE;
	}

	if (CryptGetHashParam(hHash, HP_HASHVAL, bHash, &dHash, 0)) {
		_tprintf(TEXT("Hash of file %s calculated.\n"), lSourceFile);
	}
	else {
		_ftprintf(stderr, TEXT("Error during getting hash params %d: %x.\n"), dHash, GetLastError());
		return FALSE;
	}

	hHashFile = CreateFile(lHashFile, FILE_WRITE_ACCESS, FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hHashFile == INVALID_HANDLE_VALUE) {
		_ftprintf(stderr, TEXT("Error openning file %s: %x.\n"), lHashFile, GetLastError());
		return FALSE;
	}

	if (WriteFile(hHashFile, bHash, dHash, &cbWrite, NULL))
	{
		_tprintf(TEXT("Hash saved in file %s.\n"), lHashFile);
	}
	else {
		_ftprintf(stderr, TEXT("Error writing file %s: %x.\n"), lHashFile, GetLastError());
		return FALSE;
	}

	CloseHandle(hHashFile);
	CloseHandle(hSourceFile);
	return TRUE;
}

bool CheckHash(LPTSTR lSourceFile, LPTSTR lHashFile) {
	Initialize();

	HCRYPTHASH hHash = NULL;
	HANDLE hSourceFile = INVALID_HANDLE_VALUE;
	HANDLE hHashFile = INVALID_HANDLE_VALUE;
	BYTE bBuffer[1024];
	BYTE bHash[HASH_LEN];
	BOOL bResult = FALSE;
	DWORD cbRead = 0;
	DWORD dHash = HASH_LEN;

	hSourceFile = CreateFile(lSourceFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);
	if (hSourceFile == INVALID_HANDLE_VALUE) {
		_ftprintf(stderr, TEXT("Error oopenning file %s: %x.\n"),lSourceFile,GetLastError());
		return FALSE;
	}

	// generate hash object
	if (CryptCreateHash(hCryptProv, HASH, 0, 0, &hHash)) {
		_tprintf(TEXT("Hash object has been created.\n"));
	}
	else {
		_ftprintf(stderr, TEXT("Error during creating hash object: %x.\n"), GetLastError());
		return FALSE;
	}

	while (bResult = ReadFile(hSourceFile, bBuffer, 1024, &cbRead, NULL))
	{
		if (cbRead == 0) break;

		if (!CryptHashData(hHash, bBuffer, cbRead, 0)) {
			_ftprintf(stderr, TEXT("Error during creating hash of data: %x.\n"), GetLastError());
			return FALSE;
		}
	}

	if (!bResult) {
		_ftprintf(stderr, TEXT("Error during reading file %s: %x.\n"), lSourceFile, GetLastError());
		return FALSE;
	}

	if (CryptGetHashParam(hHash, HP_HASHVAL, bHash, &dHash, 0)) {
		_tprintf(TEXT("Hash of file %s calculated.\n"), lSourceFile);
	}
	else {
		_ftprintf(stderr, TEXT("Error during getting hash params %d: %x.\n"), dHash, GetLastError());
		return FALSE;
	}

	hHashFile = CreateFile(lHashFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);
	if (hHashFile == INVALID_HANDLE_VALUE) {
		_ftprintf(stderr, TEXT("Error openning file %s: %x.\n"), lHashFile, GetLastError());
		return FALSE;
	}

	if (ReadFile(hHashFile, bBuffer, 1024, &cbRead, NULL))
	{
		BOOL bResult = FALSE;
		if (cbRead != HASH_LEN) {
			_tprintf(TEXT("Failed checking hash of file.\n"));
		}
		else {
			for (int i = 0; i < HASH_LEN; ++i) {
				bResult = (bHash[i] == bBuffer[i]);
			}
			if (bResult)
				_tprintf(TEXT("Success checking hash of file.\n"));
			else
				_tprintf(TEXT("Failed checking hash of file.\n"));
		}
	}
	else {
		_ftprintf(stderr, TEXT("Error oopenning file %s: %x.\n"), lHashFile, GetLastError());
		return FALSE;
	}

	_tprintf(TEXT("\n"));

	CloseHandle(hHashFile);
	CloseHandle(hSourceFile);
	return TRUE;
}

bool Decrypt(LPTSTR lSourceFile, LPTSTR lDestFile, LPTSTR lMasterKeyFile, LPTSTR lPassword) {
	Initialize();

	HCRYPTKEY hKey2 = NULL;
	HCRYPTKEY hKey1 = NULL;
	HCRYPTKEY hXchgKey = NULL;
	HCRYPTHASH hHash = NULL;

	PBYTE pbKeyBlob = NULL;
	DWORD dwKeyBlobLen;

	PBYTE pbBuffer = NULL;
	DWORD dwBlockLen;
	DWORD dwBufferLen;
	DWORD dwCount;

	HANDLE hSourceFile = INVALID_HANDLE_VALUE;
	HANDLE hDestinationFile = INVALID_HANDLE_VALUE;
	HANDLE hMasterKeyFile = INVALID_HANDLE_VALUE;

	// generate hash object
	if (CryptCreateHash(hCryptProv, HASH, 0, 0, &hHash)) {
		_tprintf(TEXT("Hash object has been created.\n"));
	}
	else {
		DWORD error = GetLastError();
		_ftprintf(stderr, TEXT("Error during creating hash object: %x.\n"), error);
		return FALSE;
	}

	// make hash of password
	if (CryptHashData(hHash, (BYTE*)lPassword, lstrlen(lPassword), 0)) {
		_tprintf(TEXT("The password has been hashed.\n"));
	}
	else {
		DWORD error = GetLastError();
		_ftprintf(stderr, TEXT("Error during creating hash of password: %x.\n"), error);
		return FALSE;
	}

	//generate session key based on password hash
	if (CryptDeriveKey(hCryptProv, ENCRYPT_2, hHash, KEYLENGTH, &hKey2)){
		_tprintf(TEXT("Session key generated based on password hash.\n"));
	}
	else {
		DWORD error = GetLastError();
		_ftprintf(stderr, TEXT("Error during creating session key based on password hash: %x.\n"), error);
		return FALSE;
	}

	hMasterKeyFile = CreateFile(lMasterKeyFile, FILE_READ_ACCESS, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hMasterKeyFile == INVALID_HANDLE_VALUE) {
		_tprintf(TEXT("Error openning file %s: %x.\n"), lMasterKeyFile, GetLastError());
		return FALSE;
	}

	dwKeyBlobLen = 140;
	if (!(pbKeyBlob = (PBYTE)malloc(dwKeyBlobLen))) {
		_ftprintf(stderr, TEXT("Can't allocate memory: %x.\n"), GetLastError());
		return FALSE;
	}

	if (ReadFile(hMasterKeyFile, pbKeyBlob, dwKeyBlobLen, &dwCount, NULL)) {
		_tprintf(TEXT("File for master key has been openned.\n"));
	}
	else {
		_ftprintf(stderr, TEXT("Can't read master key from file %s: %x.\n"), lMasterKeyFile, GetLastError());
		return FALSE;
	}

	dwCount = 140;
	if (CryptDecrypt(hKey2, NULL, true, 0, pbKeyBlob, &dwCount)) {
		_tprintf(TEXT("Master key decrypted.\n"));
	}
	else {
		_ftprintf(stderr, TEXT("Can't decrypt master key: %x.\n"), GetLastError());
		return FALSE;
	}

	if (!CryptImportKey(hCryptProv, pbKeyBlob, dwKeyBlobLen, 0, 0, &hKey1)) {
		_ftprintf(stderr, TEXT("Can't import master key in file: %x.\n"), GetLastError());
		return FALSE;
	}
	else {
		_tprintf(TEXT("Master key imported from %s.\n"), lMasterKeyFile);
	}

	free(pbKeyBlob);

	hSourceFile = CreateFile(lSourceFile, FILE_READ_ACCESS, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hSourceFile == INVALID_HANDLE_VALUE) {
		_ftprintf(stderr, TEXT("Can't open file to read %s: %x.\n"), lDestFile, GetLastError());
		return FALSE;
	}
	hDestinationFile = CreateFile(lDestFile, FILE_WRITE_ACCESS, FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hDestinationFile == INVALID_HANDLE_VALUE) {
		_ftprintf(stderr, TEXT("Can't open file to write %s: %x.\n"), lSourceFile, GetLastError());
		return FALSE;
	}

	dwBlockLen = ENCRYPT_BLOCK_SIZE * 10;
	dwBufferLen = dwBlockLen;
	if (pbBuffer = (PBYTE)malloc(dwBufferLen)) {
		_tprintf(TEXT("Memory for buffer allocated.\n"));
	}
	else {
		_ftprintf(stderr, TEXT("Can't allocate memory for buffer: %x.\n"), GetLastError());
		return FALSE;
	}

	bool fEOF = FALSE;
	do {
		if (!ReadFile(hSourceFile, pbBuffer, dwBlockLen, &dwCount, NULL)) {
			_ftprintf(stderr, TEXT("Can't read file %s: %x.\n"), lSourceFile, GetLastError());
			return FALSE;
		}

		if (dwCount == 0) break;

		if (dwCount < dwBlockLen) {
			fEOF = TRUE;
		}

		if (!CryptDecrypt(hKey1, 0, fEOF, 0, pbBuffer, &dwCount)) {
			_ftprintf(stderr, TEXT("Can't decrypt block: %x.\n"), GetLastError());
			return FALSE;
		}

		if (!WriteFile(hDestinationFile, pbBuffer, dwCount, &dwCount, NULL)) {
			_ftprintf(stderr, TEXT("Can't write file %s: %x.\n"), lDestFile, GetLastError());
			return FALSE;
		}
	} while (!fEOF);

	_tprintf(TEXT("Success. File %s decrypted and saved in %s.\n"), lSourceFile, lDestFile);

	CloseHandle(hSourceFile);
	CloseHandle(hDestinationFile);
	CloseHandle(hMasterKeyFile);

	return TRUE;
}





bool Encrypt(LPTSTR lSourceFile, LPTSTR lDestFile, LPTSTR lMasterKeyFile, LPTSTR lPassword) {
	Initialize();

	HCRYPTKEY hKey2 = NULL;
	HCRYPTKEY hKey1 = NULL;
	HCRYPTKEY hXchgKey = NULL;
	HCRYPTHASH hHash = NULL;

	PBYTE pbKeyBlob = NULL;
	DWORD dwKeyBlobLen;

	PBYTE pbBuffer = NULL;
	DWORD dwBlockLen;
	DWORD dwBufferLen;
	DWORD dwCount;

	HANDLE hSourceFile = INVALID_HANDLE_VALUE;
	HANDLE hDestinationFile = INVALID_HANDLE_VALUE;
	HANDLE hMasterKeyFile = INVALID_HANDLE_VALUE;

	// generate hash object
	if (CryptCreateHash(hCryptProv, HASH, 0, 0, &hHash)) {
		_tprintf(TEXT("Hash object has been created.\n"));
	}
	else {
		DWORD error = GetLastError();
		_ftprintf(stderr, TEXT("Error during creating hash object: %x.\n"), error);
		return FALSE;
	}

	// make hash of password
	if (CryptHashData(hHash, (BYTE*)lPassword, lstrlen(lPassword), 0)) {
		_tprintf(TEXT("The password has been hashed.\n"));
	}
	else {
		DWORD error = GetLastError();
		_ftprintf(stderr, TEXT("Error during creating hash of password: %x.\n"), error);
		return FALSE;
	}

	//generate session key based on password hash
	if (CryptDeriveKey(hCryptProv, ENCRYPT_2, hHash, KEYLENGTH, &hKey2)){
		_tprintf(TEXT("Session key generated based on password hash.\n"));
	}
	else {
		DWORD error = GetLastError();
		_ftprintf(stderr, TEXT("Error during creating session key based on password hash: %x.\n"), error);
		return FALSE;
	}

	
	//generate random session key for ENCRYPT_1 algorythm
	if (CryptGenKey(hCryptProv, ENCRYPT_1, CRYPT_EXPORTABLE, &hKey1)) {
		_tprintf(TEXT("A session key has been created\n"));
	}
	else {
		DWORD error = GetLastError();
		_ftprintf(stderr, TEXT("Error during creating session key: %x.\n"), error);
		return FALSE;
	}

	if (CryptGetUserKey(hCryptProv, AT_KEYEXCHANGE, &hXchgKey)) {
		_tprintf(TEXT("The user public key retreived.\n"));
	}
	else {
		DWORD error = GetLastError();
		_ftprintf(stdout, TEXT("Error during retreiving public key: %x.\nNew key will be created.\n"), error);
		if (error == NTE_NO_KEY) {
			if (!CryptGenKey(hCryptProv, AT_KEYEXCHANGE, CRYPT_EXPORTABLE, &hXchgKey)) {
				_ftprintf(stdout, TEXT("Error during creating public key: %x.\n"), GetLastError());
				return FALSE;
			}
			else {
				_tprintf(TEXT("The user public key created.\n"));
			}
		}
		else {
			_ftprintf(stdout, TEXT("User public key is not available.\n"));
			return FALSE;
		}
	}

	// get master key length
	if (CryptExportKey(hKey1, hXchgKey, SIMPLEBLOB, 0, NULL, &dwKeyBlobLen)) {
		_tprintf(TEXT("The master key blob is %d bytes long.\n"), dwKeyBlobLen);
	}
	else {
		DWORD error = GetLastError();
		_ftprintf(stderr, TEXT("Error during retreiving master key length: %x.\n"), error);
		return FALSE;
	}

	if (pbKeyBlob = (BYTE*)malloc(dwKeyBlobLen)) {
		_tprintf(TEXT("Allocated memmory for master key blob.\n"));
	}
	else {
		DWORD error = GetLastError();
		_ftprintf(stderr, TEXT("Can't allocate memmory: %x.\n"), error);
		return FALSE;
	}

	// export master key
	if (CryptExportKey(hKey1, hXchgKey, SIMPLEBLOB, 0, pbKeyBlob, &dwKeyBlobLen)) {
		_tprintf(TEXT("Master key exported.\n"));
	}
	else {
		_ftprintf(stderr, TEXT("Can't export master key: %x.\n"), GetLastError());
		return FALSE;
	}

	hMasterKeyFile = CreateFile(lMasterKeyFile, FILE_WRITE_DATA, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hMasterKeyFile == INVALID_HANDLE_VALUE) {
		_tprintf(TEXT("Error openning file %s: %x.\n"), lMasterKeyFile, GetLastError());
	}
	else {
		_tprintf(TEXT("File for master key has been openned.\n"));
	}

	dwCount = 140;
	if (CryptEncrypt(hKey2, NULL, true, 0, pbKeyBlob, &dwCount, 140)) {
		_tprintf(TEXT("Master key encrypted.\n"));
	}
	else {
		_ftprintf(stderr, TEXT("Can't encrypt master key: %x.\n"), GetLastError());
		return FALSE;
	}
	

	if (WriteFile(hMasterKeyFile, pbKeyBlob, dwKeyBlobLen, &dwCount, NULL)) {
		_tprintf(TEXT("The master key has been saved in file %s.\n"), lMasterKeyFile);
	}
	else {
		_ftprintf(stderr, TEXT("Can't save master key in file: %x.\n"), GetLastError());
		return FALSE;
	}

	free(pbKeyBlob);

	hSourceFile = CreateFile(lSourceFile, FILE_READ_ACCESS, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hSourceFile == INVALID_HANDLE_VALUE) {
		_ftprintf(stderr, TEXT("Can't open file to read %s: %x.\n"), lDestFile, GetLastError());
		return FALSE;
	}
	hDestinationFile = CreateFile(lDestFile, FILE_WRITE_ACCESS, FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hDestinationFile == INVALID_HANDLE_VALUE) {
		_ftprintf(stderr, TEXT("Can't open file to write %s: %x.\n"), lSourceFile, GetLastError());
		return FALSE;
	}

	dwBlockLen = ENCRYPT_BLOCK_SIZE * 10;
	dwBufferLen = dwBlockLen + ENCRYPT_BLOCK_SIZE;
	if (pbBuffer = (BYTE *)malloc(dwBufferLen)) {
		_tprintf(TEXT("Memory for buffer allocated.\n"));
	}
	else {
		_ftprintf(stderr, TEXT("Can't allocate memory for buffer: %x.\n"), GetLastError());
		return FALSE;
	}

	bool fEOF = FALSE;
	do {
		if (!ReadFile(hSourceFile, pbBuffer, dwBlockLen, &dwCount, NULL)) {
			_ftprintf(stderr, TEXT("Can't read file %s: %x.\n"), lSourceFile, GetLastError());
			return FALSE;
		}

		if (dwCount < dwBlockLen) {
			fEOF = TRUE;
		}

		if (!CryptEncrypt(hKey1, NULL, fEOF, 0, pbBuffer, &dwCount, dwBufferLen)) {
			_ftprintf(stderr, TEXT("Can't encrypt block: %x.\n"), GetLastError());
			return FALSE;
		}

		if (!WriteFile(hDestinationFile, pbBuffer, dwCount, &dwCount, NULL)) {
			_ftprintf(stderr, TEXT("Can't write file %s: %x.\n"),lDestFile, GetLastError());
			return FALSE;
		}
	} while (!fEOF);

	_tprintf(TEXT("Success. File %s encrypted and saved in %s.\n"), lSourceFile, lDestFile);

	CloseHandle(hSourceFile);
	CloseHandle(hDestinationFile);
	CloseHandle(hMasterKeyFile);

	return TRUE;
}

void Help() {
	_tprintf(TEXT("Usage: SoftwareSecurity_1.exe {-e, -d} <source file> <destination file> <key file> <password>"));
	_tprintf(TEXT("OR: build.exe {-c, -h} <source file> <hash file>\n"));
	_tprintf(TEXT("-e -- encrypt\n"));
	_tprintf(TEXT("-d -- decrypt\n"));
	_tprintf(TEXT("-h -- create hash\n"));
	_tprintf(TEXT("-c -- check hash\n"));
	exit(1);
}

int _tmain(int argc, _TCHAR* argv[])
{
	if (argc < 2) Help();

	LPTSTR lSource = NULL;
	LPTSTR lDestination = NULL;
	LPTSTR lKey = NULL;
	LPTSTR lPassword = NULL;
	LPTSTR lMethodName = argv[1];

	if (lstrcmp(lMethodName, L"-h") == 0) {
		if (argc != 4) Help();
		lSource = argv[2];
		lDestination = argv[3];

		MakeHash(lSource, lDestination);
		return 0;
	}

	if (lstrcmp(lMethodName, L"-c") == 0) {
		if (argc != 4) Help();
		lSource = argv[2];
		lDestination = argv[3];

		CheckHash(lSource, lDestination);
		return 0;
	}

	if (lstrcmp(lMethodName, L"-e") == 0) {
		if (argc != 6) Help();
		lSource = argv[2];
		lDestination = argv[3];
		lKey = argv[4];
		lPassword = argv[5];

		Encrypt(lSource, lDestination, lKey, lPassword);
		return 0;
	}

	if (lstrcmp(lMethodName, L"-d") == 0) {
		if (argc != 6) Help();
		lSource = argv[2];
		lDestination = argv[3];
		lKey = argv[4];
		lPassword = argv[5];

		Decrypt(lSource, lDestination, lKey, lPassword);
		return 0;
	}

	CryptReleaseContext(hCryptProv, NULL);
	Help();
	return 1;
}

