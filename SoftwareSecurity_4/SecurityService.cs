﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace SoftwareSecurity_4
{
    class SecurityService: IDisposable
    {
        private SymmetricAlgorithm symmetricAlgorithm;
        private DSACryptoServiceProvider dsa;
        private RSACryptoServiceProvider rsa;
        public static readonly byte[] salt = new byte[] { 0x00, 0x42, 0x19, 0x22, 0x43, 0x12, 0x73, 0xee };

        public SecurityService(SymmetricAlgorithm symmetricAlgorithm, DSACryptoServiceProvider dsa, RSACryptoServiceProvider rsa)
        {
            this.symmetricAlgorithm = symmetricAlgorithm;
            this.dsa = dsa;
            this.rsa = rsa;
        }

        private int RSABlockSize()
        {
            // via http://stackoverflow.com/a/3253192
            return (rsa.KeySize - 384) / 8 + 6;
        }

        public void EncryptFile(String filePath, String encryptedFilePath) {
            using (FileStream file = File.OpenRead(filePath), encrFile = File.Open(encryptedFilePath, FileMode.Create))
            {
                file.Position = 0;
                encrFile.SetLength(0);
                var buffer = new Byte[RSABlockSize()];
                Byte[] decBuffer;
                while (file.Read(buffer, 0, RSABlockSize()) != 0)
                {
                    decBuffer = rsa.Encrypt(buffer, true);
                    encrFile.Write(decBuffer, 0, decBuffer.Length);
                };
            }
        }
        public void DecryptFile(String encrFilePath, String decryptedFilePath) {
            using (FileStream encrFile = File.OpenRead(encrFilePath), decrFile = File.Open(decryptedFilePath, FileMode.Create))
            {
                encrFile.Position = 0;
                decrFile.SetLength(0);
                var buffer = new Byte[128];
                Byte[] encBuffer;
                while (encrFile.Read(buffer, 0, 128) != 0)
                {
                    // yes, i don't delete padding bytes from the end of decrypted file. So size of decrypted file will be more then file.
                    encBuffer = rsa.Decrypt(buffer, true);
                    decrFile.Write(encBuffer, 0, encBuffer.Length);
                };
            }
        }

        public void CreateCert(String filePath) {
            var rnd = new Random(DateTime.Now.Millisecond);
            var version = 1;
            var serialNo = rnd.Next();
            var signatureAlgorithm = "sha1WithDSAEncryption";
            var notAfter = DateTime.Now.AddYears(1);
            var notBefore = DateTime.Now;
            var publicKey = rsa.ToXmlString(false);

            var cert = String.Format("Data:\n\tVersion: {0}\n\tSerial Number: {1}\n\tSiganture Algorithm: {2}\n\tIssuer:\n\tValidity\n\t\tNot Before: {3}\n\t\tNot After: {4}\n\tSubject:\n\tSubject Public Key Info:\n\t\tPublic Key Algorithm: rsaEncryption\n\t\tRSA Public Key: {5}", version, serialNo, signatureAlgorithm, notBefore.ToString(), notAfter.ToString(), publicKey);
            var signature = SignString(cert);
            cert += String.Format("\nSignature Algorithm: {0}\n\t{1}", signatureAlgorithm, signature);
            File.WriteAllText(filePath, cert);
        }

        public void LoadPublicKeyFromCert(String certPath) {
            var lines = File.ReadAllLines(certPath);
            var sign = lines.Last().Trim();
            var data = String.Join("\n", lines.Take(lines.Length - 2).ToArray());
            if (CheckSignString(data, sign))
            {
                var rsaXml = lines.First(line => line.StartsWith("\t\tRSA Public Key: ")).Replace("\t\tRSA Public Key: ", "");
                rsa.FromXmlString(rsaXml);
            }
            else
            {
                throw new Exception("Certificate signature wrong");
            }
        }

        public String SignString(String data)
        {
            byte[] bData = new byte[data.Length * sizeof(char)];
            Buffer.BlockCopy(data.ToCharArray(), 0, bData, 0, bData.Length);
            var sign = dsa.SignData(bData);
            return Convert.ToBase64String(sign);
        }

        public Boolean CheckSignString(String data, String signBase64)
        {
            var sign = Convert.FromBase64String(signBase64);
            byte[] bData = new byte[data.Length * sizeof(char)];
            Buffer.BlockCopy(data.ToCharArray(), 0, bData, 0, bData.Length);
            return dsa.VerifyData(bData, sign);
        }

        public void SignFile(String filePath, String signFile) {
            using (FileStream fs = File.OpenRead(filePath), sfs = File.Open(signFile, FileMode.Create))
            {
                fs.Position = 0;
                sfs.SetLength(0);
                var signature = dsa.SignData(fs);
                sfs.Write(signature, 0, signature.Length);
            }
        }
        public Boolean CheckFileSign(String filePath, String signFile) {
            var data = File.ReadAllBytes(filePath);
            var sign = File.ReadAllBytes(signFile);

            return dsa.VerifyData(data, sign);
        }

        public void EncryptPrivateKey(String password, String keyPath) {
            var genKey = new Rfc2898DeriveBytes(password, salt, 1042);
            var cryptoTransform = symmetricAlgorithm.CreateEncryptor();
            var privateKey = rsa.ToXmlString(true);
            using (var fs = File.Open(keyPath, FileMode.Create))
            {
                using (var cs = new CryptoStream(fs, cryptoTransform, CryptoStreamMode.Write))
                {
                    using (var sw = new StreamWriter(cs))
                    {
                        sw.Write(privateKey);
                    }
                }
            }
        }

        public void DecryptPrivateKey(String password, String keyPath) {
            var genKey = new Rfc2898DeriveBytes(password, salt, 1042);
            var decryptoTransform = symmetricAlgorithm.CreateDecryptor();
            string privateKey = null;
            using (var fs = File.OpenRead(keyPath))
            {
                using (var cs = new CryptoStream(fs, decryptoTransform, CryptoStreamMode.Read))
                {
                    using (var sr = new StreamReader(cs))
                    {
                        privateKey = sr.ReadToEnd();
                        rsa.FromXmlString(privateKey);
                    }
                } 
            }
        }

        public void Dispose()
        {
            symmetricAlgorithm.Dispose();
            rsa.Dispose();
            dsa.Dispose();
        }
    }
}
