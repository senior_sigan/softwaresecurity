﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareSecurity_4
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = new SecurityService(
                SecurityFactory.BuildSymmetricAlgorithm(),
                SecurityFactory.BuildSignatureAlgorithm(),
                SecurityFactory.BuildAssymetricAlgorithm());

            service.EncryptPrivateKey("qwerty", "C:\\Users\\Ilya\\Desktop\\rsa.key");
            service.DecryptPrivateKey("qwerty", "C:\\Users\\Ilya\\Desktop\\rsa.key");

            service.EncryptFile("C:\\Users\\Ilya\\Desktop\\test.jpg", "C:\\Users\\Ilya\\Desktop\\test.jpg.enc");
            service.DecryptFile("C:\\Users\\Ilya\\Desktop\\test.jpg.enc", "C:\\Users\\Ilya\\Desktop\\test.dec.jpg");
            
            service.SignFile("C:\\Users\\Ilya\\Desktop\\test.jpg", "C:\\Users\\Ilya\\Desktop\\test.jpg.sign");
            Console.WriteLine(service.CheckFileSign("C:\\Users\\Ilya\\Desktop\\test.jpg", "C:\\Users\\Ilya\\Desktop\\test.jpg.sign"));
            
            service.CreateCert("C:\\Users\\Ilya\\Desktop\\test.cer");
            service.LoadPublicKeyFromCert("C:\\Users\\Ilya\\Desktop\\test.cer");
        }
    }
}
