﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace SoftwareSecurity_4
{
    class SecurityFactory
    {
        public static SymmetricAlgorithm BuildSymmetricAlgorithm()
        {
            return DES.Create();
        }

        public static DSACryptoServiceProvider BuildSignatureAlgorithm()
        {
            return new DSACryptoServiceProvider();
        }

        public static RSACryptoServiceProvider BuildAssymetricAlgorithm()
        {
            return new RSACryptoServiceProvider();
        }
    }
}
